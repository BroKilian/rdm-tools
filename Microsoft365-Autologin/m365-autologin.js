// Microsoft365 Autologin
// Repository: https://gitlab.com/BroKilian/rdm-tools/-/tree/main/Microsoft365-Autologin

// Variables
var apperance_to_fill_timeout = 500;
var fill_to_click_timeout = 100;

// Helper Function
function waitForElm(selector) 
{
    return new Promise(resolve => 
	{
        if (document.querySelector(selector)) 
		{
            return resolve(document.querySelector(selector));
        }

        const observer = new MutationObserver(mutations => 
		{
            if (document.querySelector(selector)) {
                observer.disconnect();
                resolve(document.querySelector(selector));
            }
        });

        observer.observe(document.body,
		{
            childList: true,
            subtree: true
        });
    });
}

// Wait for Username Field
waitForElm('#i0116').then((element) => 
{
	setTimeout(function(){
		var username_field = document.getElementById("i0116");
		username_field.value = "$USERNAME$";
		
		var username_event = new Event("change");
		username_field.dispatchEvent(username_event);
		
		var username_login_button = document.getElementById("idSIButton9");
        setTimeout(() => { username_login_button.click(); },fill_to_click_timeout);
	},apperance_to_fill_timeout);
});

// Wait for forgot password
/*
This function waits for the "Forgot Password"-Element instead of the input field.
After the username field is filled and next is clicked, the site will load of an unknown time to look up if the user might has to be redirected to an on-premise authentication server like ADFS.
But the password-input will already exist during the loading screen. So the script would try to fill it, but cant.
The "Forget Password"-Link however, will only exist if the tenant-authentication-check has already passed. So we target that instead and then fill the password field.
*/
waitForElm('#idA_PWD_ForgotPassword').then((element) => 
{
	setTimeout(function(){
		var password_field = document.getElementById("i0118");
		password_field.value = "$PASSWORD$";
		
		var password_event = new Event("change");
		password_field.dispatchEvent(password_event);
		
		var password_login_button = document.getElementById("idSIButton9");
        setTimeout(() => { password_login_button.click(); },fill_to_click_timeout);
	},apperance_to_fill_timeout);
});

// Wait for One-Time-Password
waitForElm('#idTxtBx_SAOTCC_OTC').then((element) => 
{
	setTimeout(function(){
		var otp_field = document.getElementById("idTxtBx_SAOTCC_OTC");
		otp_field.value = "$ONE_TIME_PASSWORD$";
		
		var otp_event = new Event("change");
		otp_field.dispatchEvent(otp_event);
		
		var otp_login_button = document.getElementById("idSubmit_SAOTCC_Continue");
        setTimeout(() => { otp_login_button.click(); },fill_to_click_timeout);
	},apperance_to_fill_timeout);
});